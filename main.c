#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

static int exit_flag = 0;

static void hdl(int signum, siginfo_t *siginfo, void *context) {
	//while(1)
	//	sleep(1);
	printf("SIGTERM PID: %ld, UID: %ld\n",
			(long)siginfo->si_pid, (long)siginfo->si_uid);
}

static void hdlint(int signum, siginfo_t *siginfo, void *context) {
	printf("SIGINT PID: %ld, UID: %ld\n",
			(long)siginfo->si_pid, (long)siginfo->si_uid);
}


void interrupted_sleep(int seconds) {
	while(seconds > 0) {
		seconds = sleep(seconds);
	}
}

int main(int argc, char *argv[])
{
	struct sigaction act;
	memset(&act, 0, sizeof(act));
	act.sa_sigaction = &hdl;
	act.sa_flags = SA_SIGINFO;

	if (sigaction(SIGTERM, &act, NULL) < 0) {
		perror("sigaction");
		return 1;
	}

	struct sigaction act_term;
	memset(&act_term, '\0', sizeof(act_term));
	act_term.sa_sigaction = &hdlint;
	act_term.sa_flags = SA_SIGINFO;

	if (sigaction(SIGINT, &act_term, NULL) < 0) {
		perror("sigaction");
		return 1;
	}



	while(!exit_flag)
		sleep(10);

	return 0;
}
