#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

static volatile int print_stats = 0;

size_t total_read = 0;
size_t total_write = 0;

static void hdl(int signal) {
	print_stats = 1;
}

static void do_print_stats(void) {
	if (print_stats) {
		print_stats = 0;
		fprintf(stderr, "Read: %zd, Write: %zd\n", total_read, total_write);
	}
}

int main(int argc, char *argv[])
{
	ssize_t nread;
	ssize_t nwrite;
	struct sigaction act;

	memset(&act, 0, sizeof(act));

	act.sa_handler = hdl;

	if (sigaction(SIGUSR1, &act, NULL) < 0) {
		perror("sigaction");
		return 1;
	}

	while(1) {
		char buf[512];
		nread = read(0, buf, sizeof(buf));

		if (nread < 0) {
			if (errno != EINTR) {
				perror("read");
				return 1;
			}
		}
		else if (nread == 0) {
			break;
		} else {
			ssize_t written = 0;
			total_read += nread;

			while(written < nread) {
				nwrite = write(1, buf+written, nread - written);
				if (nwrite < 0) {
					if (errno != EINTR) {
						perror("write");
						return 1;
					}
				} else {
					written += nwrite;
					total_write += nwrite;
				}
				do_print_stats();
			}
		}
	}
	
	return 0;
}
