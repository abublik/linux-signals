#include <signal.h>
#include <unistd.h>
#include <stdio.h>


static void hdl(int sig) {
	printf("sleep\n");
}

void my_sleep(int seconds) {
	while (seconds > 0) {
		seconds = sleep(seconds);
	}
}

int main(int argc, char *argv[])
{
	signal(SIGTERM, hdl);
	my_sleep(100);
	
	return 0;
}
